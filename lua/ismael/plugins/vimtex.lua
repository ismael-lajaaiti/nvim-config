vim.g.vimtex_view_method = "zathura"
vim.g.vimtex_view_general_viewer = "zathura"
vim.g.vimtex_compiler_method = "latexmk"
vim.g.vimtex_format_enabled = true
vim.cmd("let maplocalleader = ' '")
vim.cmd("set conceallevel=2")
