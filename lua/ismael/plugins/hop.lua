-- hop keymaps
vim.keymap.set("n", "<leader>hk", ":HopWordBC<cr>")
vim.keymap.set("n", "<leader>hj", ":HopWordAC<cr>")
vim.keymap.set("n", "<leader>hl", ":HopLineStart<cr>")
vim.keymap.set("n", "<leader>hp", ":HopPattern<cr>")
