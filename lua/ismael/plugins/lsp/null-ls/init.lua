local status, null_ls = pcall(require, "null-ls")
if not status then
	return
end

local julia_status, julia = pcall(require, "ismael.plugins.lsp.null-ls.julia")
if not julia_status then
	return
end

local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics
local sources = {
	julia,
	formatting.stylua,
	formatting.eslint,
    formatting.latexindent,
}

null_ls.setup({
	sources = sources,
})
