vim.cmd("let cmdline_vsplit = 1") -- split vertically
vim.cmd("let cmdline_term_width = 80") -- start with a wide window
vim.cmd("let cmdline_map_start = '<leader>st'") -- start terminal
vim.cmd("let cmdline_map_send = '<leader>sl'") -- send line to terminal
vim.cmd("let cmdline_map_send_paragraph = '<leader>sp'")
vim.cmd("let cmdline_map_send_motion = '<leader>sm'")
