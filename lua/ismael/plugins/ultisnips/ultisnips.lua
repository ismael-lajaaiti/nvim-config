vim.cmd("let g:UltiSnipsSnippetDirectories = [$HOME.'/.config/nvim/lua/ismael/plugins/ultisnips']")
vim.cmd("let g:UltiSnipsEditSplit = 'vertical'")
vim.cmd("let g:UltiSnipsExpandTrigger = ')-'")
vim.cmd("nnoremap <leader>es :UltiSnipsEdit<cr>") -- edit snippets
vim.cmd("nnoremap <leader>ea :tabedit ~/.config/nvim/lua/ismael/plugins/ultisnips/all.snippets<cr>")

-- refresh snippets
vim.keymap.set("n", "<leader>rs", ":call UltiSnips#RefreshSnippets()<CR>")
